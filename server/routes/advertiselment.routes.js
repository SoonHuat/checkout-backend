import { Router } from 'express';
import * as AdvertisementController from '../controllers/advertisement.controller';
const router = new Router();

// Get all Posts
router.route('/advertisements').get(AdvertisementController.getAdvertisements);

export default router;
