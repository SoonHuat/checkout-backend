import Advertisement from '../models/advertisement';
import cuid from 'cuid';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';

/**
 * Get all advertisements
 * @param req
 * @param res
 * @returns void
 */
export function getAdvertisements(req, res) {
  Advertisement.find().sort('-editedDate').exec((err, advertisement) => {
    if (err) {
      res.status(500).send(err);
    }
    res.json({ advertisement });
  });
}
