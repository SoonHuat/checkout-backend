import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const promotionSchema = new Schema({
  customer: { type: String },
  promoPrice: { type: Number },
  minUnitEntitlePromoPrice: { type: Number },
  bundleInputUnit: { type: Number },
  bundleOutputUnit: { type: Number }
});

const advertisementSchema = new Schema({
  id: { type: String, required: true },
  name: { type: String, required: true },
  price: { type: Number, required: true },
  promotion: [promotionSchema],
  createdDate: { type: Date, default: Date.now, required: true },
  editedDate: { type: Date, default: Date.now, required: true },
});

mongoose.model('promotion', promotionSchema);
// const advertisement = mongoose.model('advertisement', advertisementSchema);

export default mongoose.model('advertisement', advertisementSchema);
