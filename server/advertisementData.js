import Advertisement from './models/advertisement';

export default function () {
  Advertisement.count().exec((err, count) => {
    if (count > 0) {
      return;
    }

    const defaultClassic = {
      id: 'classic',
      name: 'Classic Ad',
      price: 269.99,
      promotion: [
        {
          customer: 'UNILEVER',
          promoPrice: 269.99,
          minUnitEntitlePromoPrice: 1,
          bundleInputUnit: 3,
          bundleOutputUnit: 2
        },
        {
          customer: 'FORD',
          promoPrice: 269.99,
          minUnitEntitlePromoPrice: 1,
          bundleInputUnit: 5,
          bundleOutputUnit: 4
        }
      ]
    };

    const defaultStandout = {
      id: 'standout',
      name: 'Standout Ad',
      price: 322.99,
      promotion: [
        {
          customer: 'APPLE',
          promoPrice: 299.99,
          minUnitEntitlePromoPrice: 1,
          bundleInputUnit: 1,
          bundleOutputUnit: 1
        },
        {
          customer: 'FORD',
          promoPrice: 309.99,
          minUnitEntitlePromoPrice: 1,
          bundleInputUnit: 1,
          bundleOutputUnit: 1
        }
      ]
    };

    const defaultPremium = {
      id: 'premium',
      name: 'Premium Ad',
      customer: 'default',
      price: 394.99,
      promotion: [
        {
          customer: 'NIKE',
          promoPrice: 379.99,
          minUnitEntitlePromoPrice: 4,
          bundleInputUnit: 1,
          bundleOutputUnit: 1
        },
        {
          customer: 'FORD',
          promoPrice: 389.99,
          minUnitEntitlePromoPrice: 3,
          bundleInputUnit: 1,
          bundleOutputUnit: 1,
        }
      ]
    };

    Advertisement.create([defaultClassic, defaultStandout, defaultPremium], (error) => {
      if (!error) {
        console.log('error when setting up initial advertisement package');
      }
    });
  });
}
